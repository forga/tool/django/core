from pytest import mark

from django.contrib.staticfiles import finders
from django.urls import reverse


# #############################################################################
#   core.urls
# #############################################################################
@mark.parametrize(
    "url, status, template_list",
    [
        ("home", 200, ["base.dhtml", "ribbon-mailto.dhtml"]),
        (reverse("home"), 200, ["base.dhtml"]),
        (reverse("about"), 200, ["about.dhtml", "base.dhtml"]),
        (reverse("feature"), 200, ["wip.dhtml", "base.dhtml"]),
        ("hopla", 200, ["404.dhtml", "base.dhtml"]),
    ],
)
def test_route_anon(client_anon, url, status, template_list):
    """ Granted URL access for visitor (not logged in)"""
    response = client_anon.get(url)

    assert response.status_code == status
    for template in template_list:
        assert template in [t.name for t in response.templates]


def test_route_anon_404(client_anon):
    """ URL non-access for visitor (not logged-in) """

    response = client_anon.get("admin")

    assert response.status_code == 200


# #############################################################################
#   core.static
# #############################################################################
@mark.parametrize(
    "url",
    [
        ("favico.ico"),
        ("img/favico.png"),
        ("css/styles.css"),
        ("css/knacss.css"),
        ("css/ribbon.css"),
    ],
)
def test_valid_static_files(url):
    assert finders.find(url) is not None


def test_unvalid_static_files():
    assert finders.find("css/feature.css") is None


# #############################################################################
#   core.template.context_processor
# #############################################################################
def test_context_processor_not_production(client_anon):
    """  Environment is not production """
    response = client_anon.get(reverse("home"))

    assert response.context["not_production"]
