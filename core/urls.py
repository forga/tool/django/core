from django.urls import path
from django.views.generic import TemplateView as tw


handler404 = "core.views.error_404_view"

urlpatterns = [
    path("", tw.as_view(template_name="base.dhtml"), name="home"),
    path("about", tw.as_view(template_name="about.dhtml"), name="about"),
    path("feature", tw.as_view(template_name="wip.dhtml"), name="feature"),
    path("wip", tw.as_view(template_name="wip.dhtml"), name="wip"),
]
