"""
Django settings for core project.
"""

from os import path
from os.path import dirname, join


ALLOWED_HOSTS = ["127.0.0.1"]
BASE_DIR = dirname(dirname(dirname(path.abspath(__file__))))
DEBUG = True
PROJ_DIR = dirname(dirname(path.abspath(__file__)))
SECRET_KEY = "place_here_a_valid_secret_key"

# Application definition
ROOT_URLCONF = "core.urls"
WSGI_APPLICATION = "core.wsgi.application"
INSTALLED_APPS = [
    "django.contrib.contenttypes",
    "django.contrib.staticfiles",
]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": join(BASE_DIR, "db.sqlite3"),
    }
}

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [path.join(PROJ_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "core.templates.context_processors.not_production",
            ]
        },
    }
]

# Internationalization
LANGUAGE_CODE = "fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files
STATIC_URL = "/static/"
STATICFILES_DIRS = [join(PROJ_DIR, "static")]

# Logging
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "{asctime}|{levelname}|{module}|{process:d}|{thread:d}|{message}",
            "style": "{",
        },
        "simple": {"format": "{asctime}|{levelname}|{message}", "style": "{"},
    },
    "filters": {
        "require_debug_false": {"()": "django.utils.log.RequireDebugFalse"},
        "require_debug_true": {"()": "django.utils.log.RequireDebugTrue"},
    },
    "handlers": {
        "file": {
            "class": "logging.FileHandler",
            "filename": "debug.log",
            "formatter": "verbose",
            "filters": ["require_debug_false"],
            "level": "DEBUG",
        },
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "simple",
            "filters": ["require_debug_true"],
            "level": "DEBUG",
        },
    },
    "loggers": {
        "django": {
            "handlers": ["file", "console"],
            "level": "ERROR",
            "propagate": True,
        },
        "django.request": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": False,
        },
        "django.server": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": False,
        },
        "django.db.backends": {
            "handlers": ["console"],
            "level": "ERROR",
            "propagate": False,
        },
    },
}


# Devel tools configuration
ENVIRONMENT = "devel"
INTERNAL_IPS = ["127.0.0.1", "127.0.0.1:8000"]
