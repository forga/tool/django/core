	EDITOR = geany

clean:
	# Remove files not in source control
	find . -type f -name "*.orig" -delete
	find . -type f -name "*.pyc" -delete
	find . -type f -name "debug.log" -delete
	find . -type d -name "__pycache__" -delete
	rm -rf htmlcov .pytest_cache

lint:
	${VIRTUAL_ENV}/bin/black --check .
	${VIRTUAL_ENV}/bin/flake8 --config=setup.cfg
	${VIRTUAL_ENV}/bin/pylint --rcfile=setup.cfg --load-plugins pylint_django *.py

open_all:
	${EDITOR} .gitignore .gitlab-ci.yml conftest.py Makefile README*.md requirements-dev.txt requirements.txt setup.cfg
	${EDITOR} .git/hooks/*-commit
	${EDITOR} ${VIRTUAL_ENV}/bin/activate
	${EDITOR} core/*s.py
	${EDITOR} core/settings/__init__.py
	${EDITOR} core/static/css/*s
	${EDITOR} core/templates/*l
	${EDITOR} docs/git-hooks/*

pre_commit:
	sh .git/hooks/pre-commit

rebuild_sqlite:
	rm db.sqlite3
	./manage.py migrate
	# add fixtures here
	# ./manage.py loaddata foobar_fixture
