from pytest import fixture

from django.test import Client


# #############################################################################
# ##     Fixtures
# #############################################################################
@fixture
def client_anon():
    """ Provide a Django test client with anonymous (unconnected) """
    return Client()
