`core`
======

Ce [paquet Python](https://docs.python.org/fr/3/tutorial/modules.html#packages) fait parti des [outils pour Django](https://gitlab.com/forga/tool/django/)  du groupe [`forga`](https://gitlab.com/forga/). Pour plus d'informations sur ce groupe, visitez l'URL :

https://forga.gitlab.io/fr/


💡 Pourquoi faire ?
------------------

Partager un socle de base Djanqo entre plusieurs projets.


🔧 Comment faire ?
-----------------

1. On utilise ce dépôt comme fondation du nouveau projet et on garde le lien avec le dépôt distant (_remote_) sous un nom adéquat :
```shell
git clone git@gitlab.com:forga/tool/django/core.git mon_nouveau_projet
cd mon_nouveau_projet
git remote rename origin core
```

2. On ajoute un dépôt distant (_remote_) qui recevra le code du nouveau projet.
```shell
git remote add origin git@gitlab.com:forga/customer/mon_nouveau_projet.git
```

_(Astuce : il n'est pas nécessaire de créer ce nouveau projet sur _GitLab_, _GitLab_ le fera à la première poussée : `git push origin`)_

3. On installe les dépendances dans un environnement virtuel
```shell
path/to/python3.7 -m venv ~/.venvs/mon_nouveau_projet
source ~/.venvs/mon_nouveau_projet/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

4. On peut maintenant coder la première app. du projet
```shell
./manage.py startapp mon_app
…
```

5. **Bonus :** [installer une app depuis un autre dépôt](https://gitlab.com/forga/tool/django/one_to_one/-/blob/stable/README-fr.md#installation-dans-un-projet-existant)


📦 Qu'est ce qu'il y a dedans ?
------------------------------

* un minimum de dépendances «[`trucs-django`](https://gitlab.com/forga/tool/django/core/-/blob/stable/requirements-dev.txt)»;
* du travail pour _Gitlab_ [pages & CI](https://gitlab.com/forga/tool/django/core/-/blob/stable/.gitlab-ci.yml);
* un [template HTML5/CSS](https://gitlab.com/forga/tool/django/core/-/tree/stable/core/static/css) léger ([KNACSS](https://schnaps.it/ "Générateur de template HTML5 since 1664"));
* des [`git hooks`](https://gitlab.com/forga/tool/django/core/-/blob/stable/docs/git-hooks) (à copier manuellement);
* [![_black coding style_](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black "The uncompromising code formatter");


🤝 Envie de contribuer ?
-----------------------

Avec plaisir !

Il y a quelques conventions ([git](https://forga.gitlab.io/process/fr/manuel/convention/git/) & [python](https://forga.gitlab.io/process/fr/manuel/convention/python/)) présentées dans [le manuel](https://forga.gitlab.io/process/fr/manuel/) ainsi qu'un [code de conduite](https://forga.gitlab.io/process/fr/manuel/human/code-of-conduct/).

Et pourquoi ne pas proposer une [édition de cette page](https://gitlab.com/forga/tool/django/core/-/edit/documentation/README-fr.md)?


🐘 Pour mémoire
---------------

Le code était originellement hébergé sur le dépôt [`free_zed/djbp`](https://gitlab.com/free_zed/djbp), mais l'organisation ne permettait pas vraiment de rester [DRY](https://fr.wikipedia.org/wiki/Ne_vous_r%C3%A9p%C3%A9tez_pas).
