`core`
======

[ [🇫🇷 French version](README-fr.md) ]

This [Python
package](https://docs.python.org/en/3/tutorial/modules.html#packages) is part of the [tools for Django](https://gitlab.com/forga/tool/django/) group [`forga`](https://gitlab.com/forga/). For more information about this group, visit this URL:

https://forga.gitlab.io/en/


💡 What for ?
------------

Share a Djanqo base between several projects.


🔧 How to do it ?
----------------

1. We use this repository as the foundation of the new project and keep the link to the remote repository under a suitable name :
```shell
git clone git@gitlab.com:forga/tool/django/core.git my_new_project
cd my_new_project
git remote rename origin core
```

2. We add a remote repository that will receive the code of the new project :
```shell
git remote add origin git@gitlab.com:forga/customer/my_new_project.git
```

_(Tip: it's not necessary to create this new project on _GitLab_, _GitLab_ will do it at the first push : `git push origin`)_

3. Dependencies are installed in a virtual environment :
```shell
path/to/python3.7 -m venv ~/.venvs/my_new_project
source ~/.venvs/my_new_project/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

4. We can now code the first app. of the project :
```shell
./manage.py startapp my_app
…
```


📦 What's in it ?
----------------

* a minimum of «[`foobar-django`](https://gitlab.com/forga/tool/django/core/-/blob/stable/requirements-dev.txt)» dependencies;
* work for _GitLab_ [pages & CI](https://gitlab.com/forga/tool/django/core/-/blob/stable/.gitlab-ci.yml);
* a light [HTML5/CSS](https://gitlab.com/forga/tool/django/core/-/tree/stable/core/static/css) template ([KNACSS](https://schnaps.it/ "Générateur de template HTML5 since 1664"));
* [`git hooks`](https://gitlab.com/forga/tool/django/core/-/blob/stable/docs/git-hooks) (to be copied manually);
* [![_black coding style_](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black "The uncompromising code formatter");


🤝 Want to contribute ?
----------------------

With pleasure !

There are some conventions ([git](https://forga.gitlab.io/process/fr/manuel/convention/git/) & [python](https://forga.gitlab.io/process/fr/manuel/convention/python/)) presented in [the manual](https://forga.gitlab.io/process/fr/manuel/) and a [code of conduct](https://forga.gitlab.io/process/fr/manuel/human/code-of-conduct/) (in French).

And why not propose an [edition of this page](https://gitlab.com/forga/tool/django/core/-/edit/documentation/README.md)?


🐘 For the record
-----------------

The code was originally hosted on the [`free_zed/djbp`](https://gitlab.com/free_zed/djbp) repository, but the organization didn't really allow to stay [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself).
